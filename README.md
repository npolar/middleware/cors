# CORS [![Go Report Card](https://goreportcard.com/badge/gitlab.com/npolar/middleware/cors)](https://goreportcard.com/report/gitlab.com/npolar/middleware/cors)

This package implements a simple middleware to support Cross-Origin Resource
Sharing (CORS) in Golang projects using the builtin http server. It's meant to
be used together with the [middleman](https://gitlab.com/npolar/middleware/middleman)
middleware loader.

Full API description is available at [Godoc.org](https://godoc.org/gitlab.com/npolar/middleware/cors).

|Branch|Pipeline|Coverage|
|---|---|---|
|[**Master**](https://gitlab.com/npolar/middleware/cors/tree/master)|[![pipeline status](https://gitlab.com/npolar/middleware/cors/badges/master/pipeline.svg)](https://gitlab.com/npolar/middleware/cors/commits/master)|[![coverage report](https://gitlab.com/npolar/middleware/cors/badges/master/coverage.svg)](https://gitlab.com/npolar/middleware/cors/commits/master)|
|[**Develop**](https://gitlab.com/npolar/middleware/cors/tree/develop)|[![pipeline status](https://gitlab.com/npolar/middleware/cors/badges/develop/pipeline.svg)](https://gitlab.com/npolar/middleware/cors/commits/develop)|[![coverage report](https://gitlab.com/npolar/middleware/cors/badges/develop/coverage.svg)](https://gitlab.com/npolar/middleware/cors/commits/develop)|

## Installation

Use go get to install [middleman](https://gitlab.com/npolar/middleware/middleman)
and the cors packages.

```bash
go get gitlab.com/npolar/middleware/middleman
go get gitlab.com/npolar/middleware/cors
```

## Usage

You can use middleman to load the middleware directly or as part of a larger
middleware chain.

### Direct Invocation

If you just want to use cors you can invoke it directly by name using middleman.

```go
package main

import (
    "fmt"
    "log"
    "net/http"

    _ "gitlab.com/npolar/middleware/cors"
    "gitlab.com/npolar/middleware/middleman"
)

func main() {
    // byte slice containing JSON configuration
    var opts = []byte(`{"allowMethods":["GET","HEAD","OPTIONS"]}`)

    // Invoke the cors middleware for the "/" route.
    http.HandleFunc("/", middleman.Load("corsman", MyHandler, opts))
    if err := http.ListenAndServe(":7777", nil); err != nil {
        log.Fatalln(err)
    }
}

// MyHandler returns hello world in japanese.
func MyHandler(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "text/plain;charset=UTF-8")
    w.WriteHeader(http.StatusOK)
    fmt.Fprintln(w, "こんにちは世界")
}
```
Now send a preflight request to see the Access-Control headers.
```bash
~ ❯ curl -i -XOPTIONS "http://localhost:7777/"
HTTP/1.1 200 OK
Access-Control-Allow-Headers: Accept,Content-Type,Authorization,Content-Length
Access-Control-Allow-Methods: GET,HEAD,OPTIONS
Access-Control-Allow-Origin: *
Access-Control-Expose-Headers: Content-Disposition,Content-Length,Content-Location
Access-Control-Max-Age: 86400
Date: Thu, 11 Oct 2018 14:52:40 GMT
Content-Length: 0
```
### As Part of a Chain
When using multiple middlewares at the same time it might be convenient to use
the middleman chain loader to take care of the middleware setup. This method
can be used to configure multiple middlewares at once through a config file.
```go
package main

import (
    "encoding/json"
    "fmt"
    "log"
    "net/http"

    "gitlab.com/npolar/middleware/middleman"

    _ "gitlab.com/npolar/middleware/cors"
)

func main() {
    // JSON array containing a single middleware configuration for the
    // gitlab.com/npolar/middleware/cors package. Now the middleware is
    // selected by the name it uses to register itself with middleman "corsman".
    var conf = []byte(`[{"name":"corsman","options":{` +
        `"allowCreds":true,"allowOrigins":["*","http://example.com"]}}]`)
    var chain []middleman.Conf

    // We decode the json config into a slice of middleman.Conf objects.
    err := json.Unmarshal(conf, &chain)
    if err != nil {
        log.Fatalln(err)
    }

    // Next we tell middleman to load the entire chain (cors in this example)
    // for the "/" handler.
    http.HandleFunc("/", middleman.LoadChain(MyHandler, chain))
    if err := http.ListenAndServe(":7777", nil); err != nil {
        log.Fatalln(err)
    }
}

func MyHandler(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "text/plain;charset=UTF-8")
    w.WriteHeader(http.StatusOK)
    fmt.Fprintln(w, "こんにちは世界")
}
```
Now send a preflight request with a random origin.
```bash
~ ❯ curl -i -XOPTIONS "http://localhost:7777/" -H "Origin: http://unknown.example.com"
HTTP/1.1 200 OK
Access-Control-Allow-Headers: Accept,Content-Type,Authorization,Content-Length
Access-Control-Allow-Methods: GET,HEAD,POST,DELETE,OPTIONS
Access-Control-Allow-Origin: *
Access-Control-Expose-Headers: Content-Disposition,Content-Length,Content-Location
Access-Control-Max-Age: 86400
Date: Thu, 11 Oct 2018 15:07:31 GMT
Content-Length: 0
```
Now send a preflight with a whitelisted origin.
```bash
~ ❯ curl -i -XOPTIONS "localhost:7777" -H "Origin: http://example.com"
HTTP/1.1 200 OK
Access-Control-Allow-Credentials: true
Access-Control-Allow-Headers: Accept,Content-Type,Authorization,Content-Length
Access-Control-Allow-Methods: GET,HEAD,POST,DELETE,OPTIONS
Access-Control-Allow-Origin: http://example.com
Access-Control-Expose-Headers: Content-Disposition,Content-Length,Content-Location
Access-Control-Max-Age: 86400
Vary: Origin
Date: Thu, 11 Oct 2018 15:08:24 GMT
Content-Length: 0
```
**NOTE!** The middleware supports a blend of wildcard and explicitly whitelisted
origins. This is to allow credential requests to be processed for known origins
while still allowing unknown origins to perform non auth requests. In the
unknown origin scenario the middleware will override the configured
```allowCreds``` setting to false and set the ```Access-Control-Allow-Origin```
header to a wildcard (only if wildcard is configured as one of the origin
options). This behavior is demonstrated in the above example.
### Configuration options

The following configuration options are available

| JSON key      | Type         | Default                                                       | Description                                              |
| ---           | ---          | ---                                                           | ---                                                      |
| allowCreds    | bool         | false                                                         | It's allowed to send credentials.                        |
| allowOrigins  | string array | ["\*"]                                                        | List of domains the service is accessible from.          |
| allowMethods  | string array | ["GET", "HEAD","POST","DELETE", "OPTIONS"]                    | Allowed methods                                          |
| allowHeaders  | string array | ["Accept", "Content-Type", "Authorization", "Content-Length"] | Allowed headers                                          |
| ExposeHeaders | string array | ["Content-Disposition", "Content-Length", "Content-Location"] | Expose headers                                           |
| MaxAge        | int          | 86400 (24h)                                                   | Specifies how long the preflight response can be cached. |

# Notice

Cors is still under development, API changes and bugs are likely. Usage in
production should be avoided for the time being.
