package cors

import (
	"fmt"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

const (
	message       = "Hello Cors"
	exampleOrigin = "http://example.com"
)

func Handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, message)
}

func invalidAcceptHeaderValue() string {
	var valid = []byte("abcdefghinopqrstuvwxyzABCFG/HIJKLMNO-_UVWXYZ0123456789")
	var dictionary = []byte(
		"\x22\x28\x29\x3A\x3C\x3E\x3F\x40\x5B\x5C\x5D\x7B\x7D\x7F\x10\x19",
	)
	rand.Seed(time.Now().UnixNano())
	return string(append(valid, dictionary[rand.Intn(len(dictionary))]))
}

func TestCORS(t *testing.T) {
	cl := Middleware{}.Configure(nil)
	wrappedFunc := cl.Wrap(Handler)
	t.Run("ByPass", func(t *testing.T) {
		for _, m := range []string{
			"GET", "HEAD", "POST", "PUT", "DELETE", "PATCH",
		} {
			t.Run(m, func(t *testing.T) {
				req, err := http.NewRequest(m, "/", nil)
				if err != nil {
					t.Fatal(err)
				}

				rr := httptest.NewRecorder()
				wrappedFunc.ServeHTTP(rr, req)

				if rr.Body.String() != message {
					t.Fatalf("Expected to see the default message for: %s", m)
				}
			})
		}
	})

	// Test simple request scenarios
	t.Run("simple-request", func(t *testing.T) {
		for _, m := range []string{"GET", "HEAD", "POST"} {
			req, err := http.NewRequest(m, "/", nil)
			if err != nil {
				t.Fatal(err)
			}

			t.Run(fmt.Sprintf("%s:NoHeaders", m), func(t *testing.T) {
				rr := httptest.NewRecorder()
				wrappedFunc.ServeHTTP(rr, req)
				if rr.Header().Get("Access-Control-Allow-Origin") != "*" {
					t.Fatal("Access-Control-Allow-Origin should be *")
				}
			})

			t.Run(fmt.Sprintf("%s:Invalid->Accept", m), func(t *testing.T) {
				req.Header.Set("Accept", invalidAcceptHeaderValue())
				if isSimpleRequest(req) {
					t.Fatal("Should be false with invalid header value.")
				}
			})

			t.Run(fmt.Sprintf("%s:Valid->Accept", m), func(t *testing.T) {
				req.Header.Set("Accept", "application/json\t")

				rr := httptest.NewRecorder()
				wrappedFunc.ServeHTTP(rr, req)

				if rr.Header().Get("Access-Control-Allow-Origin") != "*" {
					t.Fatal("Access-Control-Allow-Origin should be *")
				}
			})

			t.Run(fmt.Sprintf("%s:Invalid->Content-Type", m), func(t *testing.T) {
				req.Header.Set("Content-Type", "application/json")
				if m == "POST" && isSimpleRequest(req) {
					t.Fatal("Should be false for an invalid header value.")
				}
			})

			t.Run(fmt.Sprintf("%s:Valid->Content-Type", m), func(t *testing.T) {
				req.Header.Set("Content-Type", "text/plain")

				rr := httptest.NewRecorder()
				wrappedFunc.ServeHTTP(rr, req)

				if m == "POST" && rr.Header().Get("Access-Control-Allow-Origin") != "*" {
					t.Fatal("Access-Control-Allow-Origin should be *")
				}
			})

			t.Run(fmt.Sprintf("%s:Invalid->Accept-Language", m), func(t *testing.T) {
				req.Header.Set("Accept-Language", "no-nb\x40")
				if isSimpleRequest(req) {
					t.Fatal("Should be false for an invalid header value.")
				}
			})

			t.Run(fmt.Sprintf("%s:Valid->Accept-Language", m), func(t *testing.T) {
				req.Header.Set("Accept-Language", "no-nb")

				rr := httptest.NewRecorder()
				wrappedFunc.ServeHTTP(rr, req)

				if rr.Header().Get("Access-Control-Allow-Origin") != "*" {
					t.Fatal("Access-Control-Allow-Origin should be *")
				}
			})

			t.Run(fmt.Sprintf("%s:Invalid-Content-Language", m), func(t *testing.T) {
				req.Header.Set("Content-Language", "no-nb\x40")
				if isSimpleRequest(req) {
					t.Fatal("Should be false for an invalid header value.")
				}
			})

			t.Run(fmt.Sprintf("%s:Valid->Content-Language", m), func(t *testing.T) {
				req.Header.Set("Content-Language", "no-nb")

				rr := httptest.NewRecorder()
				wrappedFunc.ServeHTTP(rr, req)

				if rr.Header().Get("Access-Control-Allow-Origin") != "*" {
					t.Fatal("Access-Control-Allow-Origin should be *")
				}
			})

			t.Run(fmt.Sprintf("%s:Invalid->DPR", m), func(t *testing.T) {
				req.Header.Set("DPR", "2.o")
				if isSimpleRequest(req) {
					t.Fatal("Should be false for an invalid header value.")
				}
			})

			t.Run(fmt.Sprintf("%s:Valid->DPR", m), func(t *testing.T) {
				req.Header.Set("DPR", "5")

				rr := httptest.NewRecorder()
				wrappedFunc.ServeHTTP(rr, req)

				if rr.Header().Get("Access-Control-Allow-Origin") != "*" {
					t.Fatal("Access-Control-Allow-Origin should be *")
				}
			})

			t.Run(fmt.Sprintf("%s:Invalid->Width", m), func(t *testing.T) {
				req.Header.Set("Width", "500px")
				if isSimpleRequest(req) {
					t.Fatal("Should be false for an invalid header value.")
				}
			})

			t.Run(fmt.Sprintf("%s:Valid->Width", m), func(t *testing.T) {
				req.Header.Set("Width", "320")

				rr := httptest.NewRecorder()
				wrappedFunc.ServeHTTP(rr, req)

				if rr.Header().Get("Access-Control-Allow-Origin") != "*" {
					t.Fatal("Access-Control-Allow-Origin should be *")
				}
			})

			t.Run(fmt.Sprintf("%s:Invalid->Viewport-Width", m), func(t *testing.T) {
				req.Header.Set("Viewport-Width", "320px")
				if isSimpleRequest(req) {
					t.Fatal("Should be false for an invalid header value")
				}
			})

			t.Run(fmt.Sprintf("%s:Valid->Viewport-Width", m), func(t *testing.T) {
				req.Header.Set("Viewport-Width", "320")

				rr := httptest.NewRecorder()
				wrappedFunc.ServeHTTP(rr, req)

				if rr.Header().Get("Access-Control-Allow-Origin") != "*" {
					t.Fatal("Access-Control-Allow-Origin should be *")
				}
			})

			t.Run(fmt.Sprintf("%s:Invalid->Save-Data", m), func(t *testing.T) {
				req.Header.Set("Save-Data", "true")
				if isSimpleRequest(req) {
					t.Fatal("Should be false for an invalid header value.")
				}
			})

			t.Run(fmt.Sprintf("%s:Valid->Save-data", m), func(t *testing.T) {
				req.Header.Set("Save-Data", "on")

				rr := httptest.NewRecorder()
				wrappedFunc.ServeHTTP(rr, req)

				if rr.Header().Get("Access-Control-Allow-Origin") != "*" {
					t.Fatal("Access-Control-Allow-Origin should be *")
				}
			})

			t.Run(fmt.Sprintf("%s:Invalid->Header_Length", m), func(t *testing.T) {
				var toolong []byte
				for i := 0; i < 129; i++ {
					toolong = append(toolong, []byte("a")...)
				}
				req.Header.Set("Accept", string(toolong))
				if isSimpleRequest(req) {
					t.Fatal("Should be false for an invalid header value.")
				}

				req.Header.Set("Accept", "text/plain")
			})
		}
	})

	// Setup a test Preflight Request
	req, err := http.NewRequest("OPTIONS", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	t.Run("Defaults", func(t *testing.T) {
		rr := httptest.NewRecorder()
		wrappedFunc.ServeHTTP(rr, req)

		def := NewCors()

		for hdr, val := range map[string]interface{}{
			"Access-Control-Allow-Origin":   def.AllowOrigins[0],
			"Access-Control-Allow-Headers":  strings.Join(def.AllowHeaders, ","),
			"Access-Control-Allow-Methods":  strings.Join(def.AllowMethods, ","),
			"Access-Control-Expose-Headers": strings.Join(def.ExposeHeaders, ","),
			"Access-Control-Max-Age":        fmt.Sprintf("%d", def.MaxAge),
		} {
			if rr.Header().Get(hdr) != val {
				t.Fatal("Expected to get default Access-Control headers")
			}
		}
	})

	req.Header.Set("Origin", exampleOrigin)
	t.Run("Creds", func(t *testing.T) {
		// When creds is true wildcard origin isn't allowed. If no origin
		// is present in the request we return null. if one is present we
		// spoof the request origin, this is unsafe and should be used with
		// caution! But in some cases is the only way to make things work.
		t.Run("true", func(t *testing.T) {
			wrappedFunc = Middleware{}.Configure(
				[]byte(fmt.Sprintf(`{"AllowCreds":"%v"}`, true)),
			).Wrap(Handler)

			rr := httptest.NewRecorder()
			wrappedFunc.ServeHTTP(rr, req)

			if rr.Header().Get("Access-Control-Allow-Credentials") != "" {
				t.Fatalf("Allow Creds should be undefined for wildcard matches.")
			}
		})
	})

	t.Run("Custom", func(t *testing.T) {
		wrappedFunc = Middleware{}.Configure(
			[]byte(fmt.Sprintf(
				`{"allowOrigins":["%s"], "allowCreds": %v}`,
				exampleOrigin, true,
			)),
		).Wrap(Handler)

		rr := httptest.NewRecorder()
		wrappedFunc.ServeHTTP(rr, req)

		if o := rr.Header().Get("Access-Control-Allow-Origin"); o != exampleOrigin {
			t.Fatalf("Expected configured origin to be used but got -> %s", o)
		}

		if rr.Header().Get("Access-Control-Allow-Credentials") != "true" {
			t.Fatal("It should respect creds settings for whitelisted addresses.")
		}

		if rr.Header().Get("Vary") != "Origin" {
			t.Fatal("It should add a Vary header when matching an exact origin.")
		}
	})

}
