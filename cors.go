// Package cors provides a simple Cross Origin Resource Sharing middleware for
// the middleman middleware loader.
package cors

import (
	"encoding/json"
	"fmt"
	"mime"
	"net/http"
	"strings"

	"gitlab.com/npolar/middleware/middleman"
)

const name = "corsman"

// Middleware serves as an anchor for our configuration logic.
type Middleware struct{}

// Configure a new Cors instance with the provided json configuration.
func (m Middleware) Configure(opts json.RawMessage) middleman.Link {
	var c = NewCors()
	json.Unmarshal(opts, c)
	return c
}

// Cors defines common cross origin header fields.
type Cors struct {
	AllowCreds    bool     `json:"allowCreds,omitempty"`
	AllowOrigins  []string `json:"allowOrigins,omitempty"`
	AllowMethods  []string `json:"allowMethods,omitempty"`
	AllowHeaders  []string `json:"allowHeaders,omitempty"`
	ExposeHeaders []string `json:"exposeHeaders,omitempty"`
	MaxAge        int      `json:"origin,omitempty"`
}

// NewCors returns a new Cors reference initialized with some sane defaults.
func NewCors() *Cors {
	return &Cors{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{
			"GET",
			"HEAD",
			"PUT",
			"POST",
			"DELETE",
			"OPTIONS",
		},
		AllowHeaders: []string{
			"Accept",
			"Content-Type",
			"Authorization",
			"Content-Length",
		},
		ExposeHeaders: []string{
			"Content-Disposition",
			"Content-Length",
			"Content-Location",
		},
		MaxAge: 86400, // Allow the preflight to be cached for 24 hours
	}
}

// Wrap the original HandlerFunc with the CORS logic. For an OPTIONS request the
// middleware will set the correct Access-Control headers and return a response
// to the caller. For any other method the original HandlerFunc is invoked.
func (c *Cors) Wrap(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Grab the requests Origin header and check if it matches any of
		// the configured allowed origins.
		var creds bool
		var allowedOrigin = "null"
		var origin = r.Header.Get("Origin")
		for _, o := range c.AllowOrigins {
			if o == origin {
				allowedOrigin = o
				creds = true
				break
			} else if o == "*" {
				allowedOrigin = o
				creds = false
			}
		}
		// If the allowedOrigin is a specific host we add the Vary header
		// with the Origin value.
		if allowedOrigin != "*" && allowedOrigin != "null" {
			w.Header().Set("Vary", "Origin")
		}
		// Only respond with CORS info on a preflight request.
		if r.Method == "OPTIONS" {
			// Set the correct preflight headers
			w.Header().Set("Access-Control-Allow-Origin", allowedOrigin)
			w.Header().Set("Access-Control-Allow-Methods", strings.Join(c.AllowMethods, ","))
			w.Header().Set("Access-Control-Allow-Headers", strings.Join(c.AllowHeaders, ","))
			w.Header().Set("Access-Control-Expose-Headers", strings.Join(c.ExposeHeaders, ","))
			w.Header().Set("Access-Control-Max-Age", fmt.Sprintf("%d", c.MaxAge))
			// Set the allow Credentials header. Only valid value is true
			// otherwise the header should be omited.
			if c.AllowCreds && creds {
				w.Header().Set("Access-Control-Allow-Credentials", "true")
			}
		} else if isSimpleRequest(r) {
			// In case of a simple request return a wildcard for the
			// Access-Control-Allow-Origin header.
			w.Header().Set("Access-Control-Allow-Origin", "*")
			fn(w, r)
		} else {
			if allowedOrigin != "null" {
				w.Header().Set("Access-Control-Allow-Origin", allowedOrigin)
			}
			fn(w, r) // If not a preflight request call the original HandlerFunc.
		}
	}
}

// safeListRequestHeadersChecks defines a map containing the CORS-safelist
// request headers as key and a function that validates the header value as
// defined in the Fetch spec.
//	https://fetch.spec.whatwg.org/#cors-safelisted-request-header
var safeListRequestHeadersChecks = map[string]func(string) bool{
	"Accept":           validAcceptValue,
	"Accept-Language":  validLanguageValue,
	"Content-Language": validLanguageValue,
	"Content-Type":     validContentType,
	"DPR":              validDPR,
	"Save-Data":        validSaveDataToken,
	"Viewport-Width":   validWidth,
	"Width":            validWidth,
}

// isSimpleRequest checks if the request should be handled as a CORS simple
// request. In this case the response will contain a Access-Control-Allow-Origin
// header containing the * wildcard value.
//	https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS#Simple_requests
func isSimpleRequest(r *http.Request) bool {
	var simpleRequest = r.Method == "GET" ||
		r.Method == "HEAD" ||
		r.Method == "POST"

	if simpleRequest {
		var val string
		for hdr, check := range safeListRequestHeadersChecks {
			if val = r.Header.Get(hdr); val != "" {
				// Value can't be more than 128 characters
				simpleRequest = len(val) <= 128 && check(val)
				if !simpleRequest {
					return simpleRequest
				}
			}
		}
	}

	return simpleRequest
}

// Simple requests can be a POST when it is the content-type header should
// contain one of the following values:
//	- text/plain
//	- multipart/form-data
//	- application/x-www-form-urlencoded
func validContentType(val string) bool {
	t, _, _ := mime.ParseMediaType(val)
	return t == "multipart/form-data" || t == "text/plain" ||
		t == "application/x-www-form-urlencoded"
}

// Byte slice holding the byte value of characters that are illegal for usage
// in the Accept header value. As defined by the fetch spec:
//	https://fetch.spec.whatwg.org/#cors-safelisted-request-header
var illegalAcceptBytes = []byte{
	0x22, 0x28, 0x29, 0x3A, 0x3C, 0x3E, 0x3F, // "():<>?
	0x40, 0x5B, 0x5C, 0x5D, 0x7B, 0x7D, 0x7F, // @[\]{}DEL
}

// validAcceptValue checks if the value doesn't contain any illegal characters
// defined in the illegalAcceptBytes variable.
func validAcceptValue(val string) bool {
	var reject bool
	for _, vb := range []byte(val) {
		if reject = vb < 0x20 && vb != 0x09; reject {
			return false
		}
		for _, ib := range illegalAcceptBytes {
			if reject = vb == ib; reject {
				return false
			}
		}
	}
	return true
}

// validLangBytes is a slice holding the byte value of characters that are valid
// for usage in the Accept-Language and Content-Language header value. As
// defined by the fetch spec:
//	https://fetch.spec.whatwg.org/#cors-safelisted-request-header
var validLangBytes = []byte{
	0x20, 0x2A, 0x2C, 0x2D, 0x2E, 0x3B, 0x3D, // SP*,-.;=
}

// validLanguage checks if the contents are within the valid byte ranges:
//	- 0x30 - 0x39 [0-9]
//	- 0x41 - 0x5A [A-Z]
//	- 0x61 - 0x7A [a-z]
// or are one of the bytes defined in validByteLang
func validLanguageValue(val string) bool {
	var valid bool
	for _, vb := range []byte(val) {
		valid = (vb >= 0x30 && vb <= 0x39) ||
			(vb >= 0x41 && vb <= 0x5A) ||
			(vb >= 0x61 && vb <= 0x7A)
		if !valid {
			for _, ib := range validLangBytes {
				if valid = vb == ib; valid {
					break // Once we match a char move to the next val
				}
			}
		}
	}
	return valid
}

// validDPR checks if the DPR header value is a decimal number.
func validDPR(val string) bool {
	var valid bool
	for _, vb := range []byte(val) {
		// Byte isn't a digit in range "0-9" or a "."
		if valid = (vb >= 0x30 && vb <= 0x39) || vb == 0x2E; !valid {
			return valid
		}
	}
	return valid
}

// validWidth checks if the value contains an integer.
func validWidth(val string) bool {
	var valid bool
	for _, vb := range []byte(val) {
		// Byte isn't a digit in range "0-9"
		if valid = vb >= 0x30 && vb <= 0x39; !valid {
			return valid
		}
	}
	return valid
}

// validSaveDataToken checks if the sd-token value is "on"
func validSaveDataToken(val string) bool {
	return val == "on"
}

func init() {
	middleman.RegisterMiddleware(name, Middleware{})
}
